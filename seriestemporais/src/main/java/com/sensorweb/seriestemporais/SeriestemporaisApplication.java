package com.sensorweb.seriestemporais;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeriestemporaisApplication {



	public static void main(String[] args) {
		SpringApplication.run(SeriestemporaisApplication.class, args);

	}

}
