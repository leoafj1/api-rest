package com.sensorweb.seriestemporais.model;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DadosTemporais implements Serializable {
    private static final long serialVersionUID = 1L;

    private String xid;
    private String nomePonto;
    private String ts;
    private Double dado;


}