package com.sensorweb.seriestemporais.controller;

import com.sensorweb.seriestemporais.model.DadosTemporais;
import com.sensorweb.seriestemporais.service.DadosTemporaisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
    @RequestMapping(value = "/api/armazem03/v1")
    public class DadosTemporaisController {

        @Autowired
        DadosTemporaisService dadosTemporaisService;

        @GetMapping
        public List<DadosTemporais> dadosArmazem03() {

            return  dadosTemporaisService.lerArquivo();
        }

    }

