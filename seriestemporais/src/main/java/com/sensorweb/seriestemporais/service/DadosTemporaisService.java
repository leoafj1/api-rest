package com.sensorweb.seriestemporais.service;

import com.sensorweb.seriestemporais.model.DadosTemporais;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class DadosTemporaisService {

    public List<DadosTemporais> lerArquivo() {

        List<DadosTemporais> list = new ArrayList<>();

            try {
                XSSFWorkbook work = new XSSFWorkbook(new FileInputStream("C:\\Users\\Leonardo\\Desktop\\dados\\dadosdoponto.xlsx"));
                XSSFSheet sheet = work.getSheet("dadosponto");


                for (int i = 1; i < sheet.getLastRowNum() +1 ; i++) {
                    XSSFRow row = sheet.getRow(i);
                    list.addAll(armazem03(row.getCell(0).getStringCellValue(), row.getCell(1).getStringCellValue()));
                }

            }catch (IOException e) {
                e.printStackTrace();
            }

            return list;
        }

    public List<DadosTemporais> armazem03(String xid, String nome) {

        String path = "C:\\Users\\Leonardo\\Desktop\\dados\\";
        String extention = ".csv";

        List<DadosTemporais> list = new ArrayList<DadosTemporais>();

        try (BufferedReader br = new BufferedReader(new FileReader(path + nome + extention))) {

            String line = br.readLine();
            line = br.readLine();
            while (line != null) {

                String[] vect = line.split(",");
                String ts = vect[0];
                Double dado = Double.parseDouble(vect[1]);

                DadosTemporais dadosTemporais = new DadosTemporais(xid, nome ,ts, dado);
                list.add(dadosTemporais);

                line = br.readLine();
            }


        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }

        return list;
    }
}
